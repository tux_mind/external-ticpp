cmake_minimum_required (VERSION 2.6 FATAL_ERROR)

project (libticpp)

set (TICPP_SRC
	tinyxml
	tinyxmlparser
	tinystr
	tinyxmlerror
	ticpp
)

include_directories (
	${PROJECT_SOURCE_DIR}
)

add_definitions(
	-DUNIX -DTIXML_USE_TICPP -DNDEBUG -O3 -fPIC
)

# Library
add_library(
	ticpp STATIC
	${TICPP_SRC}
)

# Install
set_property (
	TARGET ticpp
	PROPERTY PUBLIC_HEADER
	${PROJECT_SOURCE_DIR}/ticpp.h
	${PROJECT_SOURCE_DIR}/ticpprc.h
	${PROJECT_SOURCE_DIR}/tinyxml.h
)
install (
	TARGETS ticpp
	DESTINATION "lib"
	PUBLIC_HEADER DESTINATION "include"
)

# Uninstall
configure_file(
	"${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
	"${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
	IMMEDIATE @ONLY
)

add_custom_target(
	uninstall
	COMMAND ${CMAKE_COMMAND}
	-P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake
)

