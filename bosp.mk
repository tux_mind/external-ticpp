
ifdef CONFIG_EXTERNAL_TICPP

# Targets provided by this project
.PHONY: ticpp clean_ticpp

# Add this to the "external" target
external: ticpp
clean_external: clean_ticpp

MODULE_DIR_TICPP=external/required/ticpp

ticpp: setup $(BUILD_DIR)/lib/libticpp.a
$(BUILD_DIR)/lib/libticpp.a:
	@echo
	@echo "==== Building TiCPP Library ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_DIR_TICPP)/build/$(BUILD_TYPE) ] || \
	        mkdir -p $(MODULE_DIR_TICPP)/build/$(BUILD_TYPE) || \
	        exit 1
	@cd $(MODULE_DIR_TICPP)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
	        cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
	        exit 1
	@cd $(MODULE_DIR_TICPP)/build/$(BUILD_TYPE) && \
	        make -j$(CPUS) install || \
	        exit 1

clean_ticpp:
	@echo "==== Clean-up TiCPP library v1.0.1 ===="
	@[ ! -d $(MODULE_DIR_TICPP)/build/Debug ] || \
		make -C $(MODULE_DIR_TICPP)/build/Debug clean uninstall
	@[ ! -d $(MODULE_DIR_TICPP)/build/Release ] || \
		make -C $(MODULE_DIR_TICPP)/build/Release clean uninstall
	@[ ! -d $(MODULE_DIR_TICPP)/build ] || \
		rm -rf $(MODULE_DIR_TICPP)/build

else # CONFIG_EXTERNAL_TICPP

ticpp:
	$(warning $(MODULE_DIR_TICPP) module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_TICPP

